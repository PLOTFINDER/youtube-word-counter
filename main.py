from youtube_transcript_api import YouTubeTranscriptApi
from findVideos import *
import json

video_ids = []
videos = get_all_video_in_channel("UCNqN7k7qUKVhYQb5DLEDdaA")
for video in videos:
    video_ids.append(video.replace("https://www.youtube.com/watch?v=", ""))

word_dict = {}


def countWords(id):
    try:
        transcript_list = YouTubeTranscriptApi.get_transcript(id, languages=['fr'])
        text = ""
        for transcript in transcript_list:
            text = text + " " + transcript["text"]

        word_list = text.split(" ")

        unique_word_list = list(set(word_list))

        for word in unique_word_list:

            if word in word_dict.keys():
                word_dict[word] += word_list.count(word)
            else:
                word_dict[word] = word_list.count(word)

    except:
        print("nop")


for i, id in enumerate(video_ids):
    print(f"{i+1} / {len(video_ids)}")
    countWords(id)

sorted_words_list = sorted(word_dict.items(), key=lambda x: x[1], reverse=True)
sorted_words = {}
for i in sorted_words_list:
    sorted_words[i[0]] = i[1]

with open("sardoche.json", 'w') as f:
    f.write(json.dumps(sorted_words, indent=4))
    f.close()
